package com.gitlab.akluball.chessapp.user.itest.security;

import com.gitlab.akluball.chessapp.user.itest.TestConfig;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.gitlab.akluball.chessapp.user.itest.util.Util.asRuntime;

@Singleton
public class UserSecurity {
    private final RSASSASigner signer;
    private final RSASSAVerifier verifier;

    @Inject
    UserSecurity(TestConfig testConfig) {
        this.signer = new RSASSASigner(testConfig.userPrivateKey());
        this.verifier = new RSASSAVerifier(testConfig.userPublicKey());
    }

    private String createToken(User user, Instant expiration) {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256)
                .build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + user.getId())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.USER.asString()))
                .expirationTime(Date.from(expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(this.signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        return token.serialize();
    }

    public String tokenFor(User user) {
        return this.createToken(user, Instant.now().plusSeconds(60));
    }

    public String expiredTokenFor(User user) {
        return this.createToken(user, Instant.now().minusSeconds(60));
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }

    public int getUserId(SignedJWT token) {
        try {
            return Integer.parseInt(token.getJWTClaimsSet().getSubject());
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public Instant getExpiration(SignedJWT token) {
        try {
            return token.getJWTClaimsSet().getExpirationTime().toInstant();
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }

    public List<BearerRole> getRoles(SignedJWT token) {
        try {
            return token.getJWTClaimsSet().getStringListClaim(BearerRole.CLAIM_NAME)
                    .stream()
                    .map(BearerRole::fromString)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }
}
