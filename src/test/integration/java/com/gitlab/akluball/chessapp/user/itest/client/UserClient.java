package com.gitlab.akluball.chessapp.user.itest.client;

import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.transfer.GameResultDto;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.user.itest.security.SecurityUtil.bearer;

public class UserClient {
    private final WebTarget target;

    @Inject
    UserClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("user");
    }

    public Response getCurrentUser(String token) {
        Invocation.Builder invocationBuilder = this.target.request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response findById(String token, String userId) {
        Invocation.Builder invocationBuilder = this.target.path(userId).request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response findById(String token, User user) {
        return this.findById(token, "" + user.getId());
    }

    public Response search(String token, String searchString) {
        Invocation.Builder invocationBuilder = this.target.path("search")
                .queryParam("searchString", searchString)
                .request();
        bearer(invocationBuilder, token);
        return invocationBuilder.get();
    }

    public Response endGame(String token, GameResultDto gameResultDto) {
        Invocation.Builder invocationBuilder = this.target.path("endgame").request();
        bearer(invocationBuilder, token);
        return invocationBuilder
                .post(Objects.nonNull(gameResultDto) ? Entity.json(gameResultDto) : Entity.json("null"));
    }
}
