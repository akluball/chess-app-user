package com.gitlab.akluball.chessapp.user.itest.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Access(AccessType.FIELD)
public class Session {
    @Id
    @GeneratedValue
    private int id;
    private String sessionKey;
    @AttributeOverride(name = "epochSecond", column = @Column(name = "expEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "expNano"))
    private PersistableInstant expiration;
    @AttributeOverride(name = "epochSecond", column = @Column(name = "tokenExpEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "tokenExpNano"))
    private PersistableInstant freshestTokenExpiration;

    public boolean isSessionKey(String sessionKey) {
        return this.sessionKey.equals(sessionKey);
    }

    public String getSessionKey() {
        return this.sessionKey;
    }

    public PersistableInstant getExpiration() {
        return this.expiration;
    }

    public PersistableInstant getFreshestTokenExpiration() {
        return freshestTokenExpiration;
    }

    public static class Builder {
        private final Session session;

        Builder() {
            this.session = new Session();
            this.session.freshestTokenExpiration = PersistableInstant.from(Instant.now());
        }

        public Builder sessionKey(String sessionKey) {
            this.session.sessionKey = sessionKey;
            return this;
        }

        public Builder expiration(Instant expiration) {
            this.session.expiration = PersistableInstant.from(expiration);
            return this;
        }

        public Session build() {
            return this.session;
        }
    }
}