package com.gitlab.akluball.chessapp.user.itest.client;

import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.security.CookieNames;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class TokenClient {
    private final WebTarget target;

    @Inject
    TokenClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("token");
    }

    public Response getToken(String userId, String sessionKey) {
        Invocation.Builder invocationBuilder = this.target.request();
        if (Objects.nonNull(userId)) {
            invocationBuilder.cookie(CookieNames.USER_ID_TOKEN, userId);
        }
        if (Objects.nonNull(sessionKey)) {
            invocationBuilder.cookie(CookieNames.SESSION_TOKEN, sessionKey);
        }
        return invocationBuilder.get();
    }

    public Response getToken(User user, Session session) {
        String userId = Objects.nonNull(user) ? "" + user.getId() : null;
        String sessionKey = Objects.nonNull(session) ? session.getSessionKey() : null;
        return this.getToken(userId, sessionKey);
    }
}
