package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.LoginClient;
import com.gitlab.akluball.chessapp.user.itest.client.LogoutClient;
import com.gitlab.akluball.chessapp.user.itest.client.TokenClient;
import com.gitlab.akluball.chessapp.user.itest.client.UserClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.security.CookieNames;
import com.gitlab.akluball.chessapp.user.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.user.itest.transfer.GameResultDto;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.user.itest.util.Builders;
import com.google.common.collect.Range;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.NewCookie;
import javax.ws.rs.core.Response;
import java.time.Instant;

import static com.gitlab.akluball.chessapp.user.itest.util.Util.buffer5Seconds;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserIT {
    private final Builders builders;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final GameHistorySecurity gameHistorySecurity;
    private final LoginClient loginClient;
    private final TokenClient tokenClient;
    private final LogoutClient logoutClient;
    private final UserClient userClient;

    @Guicy
    UserIT(Builders builders,
            UniqueBuilders uniqueBuilders,
            UserDao userDao,
            GameHistorySecurity gameHistorySecurity,
            LoginClient loginClient,
            TokenClient tokenClient,
            LogoutClient logoutClient,
            UserClient userClient) {
        this.builders = builders;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.gameHistorySecurity = gameHistorySecurity;
        this.loginClient = loginClient;
        this.tokenClient = tokenClient;
        this.logoutClient = logoutClient;
        this.userClient = userClient;
    }

    private User createPersistedUser() {
        User user = this.uniqueBuilders.user().build();
        this.userDao.persist(user);
        return user;
    }

    @Test
    @Guicy
    void loginPersistsSessionCookie(UniqueData uniqueData) {
        String password = uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user, password);
        NewCookie sessionCookie = response.getCookies().get(CookieNames.SESSION_TOKEN);
        this.userDao.refresh(user);
        assertThat(user.getSession(sessionCookie)).isNotNull();
    }

    @Test
    @Guicy
    void loginSetsSessionCookieExpiration(UniqueData uniqueData, TestConfig testConfig) {
        String password = uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user, password);
        NewCookie sessionCookie = response.getCookies().get(CookieNames.SESSION_TOKEN);
        this.userDao.refresh(user);
        Range<Instant> expirationRange = buffer5Seconds(Instant.now().plusSeconds(testConfig.sessionLifeSeconds()));
        assertThat(user.getSession(sessionCookie).getExpiration().toInstant()).isIn(expirationRange);
    }

    @Test
    @Guicy
    void getTokenPersistsFreshestTokenExpiration(TestConfig testConfig) {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        this.tokenClient.getToken(user, session);
        this.userDao.refresh(session);
        Range<Instant> expirationRange = buffer5Seconds(Instant.now().plusSeconds(testConfig.tokenLifeSeconds()));
        assertThat(session.getFreshestTokenExpiration().toInstant()).isIn(expirationRange);
    }

    @Test
    void logoutDeletesSession() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        this.logoutClient.logout(user, session);
        this.userDao.refresh(user);
        assertThat(user.getSessions()).doesNotContain(session);
    }

    @Test
    void endGameWhiteSideRating() {
        User whiteSideUser = this.uniqueBuilders.user().rating(500).build();
        this.userDao.persist(whiteSideUser);
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(whiteSideUser).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(12).blackSideUserRatingAdjustment(0).build();
        this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        this.userDao.refresh(whiteSideUser);
        assertThat(whiteSideUser.getRating()).isEqualTo(512);
    }

    @Test
    void endGameBlackSideRating() {
        User blackSideUser = this.uniqueBuilders.user().rating(500).build();
        this.userDao.persist(blackSideUser);
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .blackSideUser(blackSideUser).whiteSideUser(this.createPersistedUser())
                .blackSideUserRatingAdjustment(-15).whiteSideUserRatingAdjustment(0).build();
        this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        this.userDao.refresh(blackSideUser);
        assertThat(blackSideUser.getRating()).isEqualTo(485);
    }
}
