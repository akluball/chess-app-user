package com.gitlab.akluball.chessapp.user.itest.client;

import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.security.CookieNames;

import javax.inject.Inject;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class LogoutClient {
    private final WebTarget target;

    @Inject
    LogoutClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("logout");
    }

    public Response logout(String userId, String sessionKey) {
        Invocation.Builder invocationBuilder = this.target.request();
        if (Objects.nonNull(userId)) {
            invocationBuilder.cookie(CookieNames.USER_ID_LOGOUT, userId);
        }
        if (Objects.nonNull(sessionKey)) {
            invocationBuilder.cookie(CookieNames.SESSION_LOGOUT, sessionKey);
        }
        return invocationBuilder.post(null);
    }

    public Response logout(User user, Session session) {
        return this.logout("" + user.getId(), session.getSessionKey());
    }
}
