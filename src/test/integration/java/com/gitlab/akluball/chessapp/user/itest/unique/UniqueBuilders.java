package com.gitlab.akluball.chessapp.user.itest.unique;

import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.util.Builders;

import javax.inject.Inject;
import java.time.Instant;

public class UniqueBuilders {
    private final UniqueData uniqueData;
    private final Builders builders;

    @Inject
    UniqueBuilders(UniqueData uniqueData, Builders builders) {
        this.uniqueData = uniqueData;
        this.builders = builders;
    }

    public User.Builder user() {
        return this.builders.user()
                .handle(this.uniqueData.handle());
    }

    public Session.Builder session() {
        return this.builders.session().sessionKey(uniqueData.sessionKey()).expiration(Instant.now().plusSeconds(60));
    }
}
