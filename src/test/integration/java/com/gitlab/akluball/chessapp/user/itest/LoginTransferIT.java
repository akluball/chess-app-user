package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.LoginClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.security.CookieNames;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.NewCookie;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class LoginTransferIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final LoginClient loginClient;
    private final TestConfig testConfig;

    @Guicy
    LoginTransferIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            UserDao userDao,
            LoginClient loginClient,
            TestConfig testConfig) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.loginClient = loginClient;
        this.testConfig = testConfig;
    }

    @Test
    void loginUserIdTokenCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_TOKEN);
        assertThat(userIdCookie.getValue()).isEqualTo("" + user.getId());
    }

    @Test
    void loginUserIdTokenCookieMaxAge() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_TOKEN);
        assertThat(userIdCookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
    }

    @Test
    void loginUserIdTokenCookiePath() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_TOKEN);
        assertThat(userIdCookie.getPath()).isEqualTo(String.format("/%s/api/token", this.testConfig.userDeployName()));
    }

    @Test
    void loginUserIdTokenCookieHttpOnly() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_TOKEN);
        assertThat(userIdCookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginSessionTokenCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies()
                .get(CookieNames.SESSION_TOKEN);
        assertThat(sessionCookie.getValue()).isNotEmpty();
    }

    @Test
    void loginSessionTokenCookieMaxAge() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies()
                .get(CookieNames.SESSION_TOKEN);
        assertThat(sessionCookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
    }

    @Test
    void loginSessionTokenCookiePath() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies()
                .get(CookieNames.SESSION_TOKEN);
        assertThat(sessionCookie.getPath()).isEqualTo(String.format("/%s/api/token", this.testConfig.userDeployName()));
    }

    @Test
    void loginSessionTokenCookieHttpOnly() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies()
                .get(CookieNames.SESSION_TOKEN);
        assertThat(sessionCookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginUserIdLogoutCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_LOGOUT);
        assertThat(userIdCookie.getValue()).isEqualTo("" + user.getId());
    }

    @Test
    void loginUserIdLogoutCookieMaxAge() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_LOGOUT);
        assertThat(sessionCookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
    }

    @Test
    void loginUserIdLogoutCookiePath() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_LOGOUT);
        assertThat(sessionCookie.getPath())
                .isEqualTo(String.format("/%s/api/logout", this.testConfig.userDeployName()));
    }

    @Test
    void loginUserIdLogoutCookieHttpOnly() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.USER_ID_LOGOUT);
        assertThat(sessionCookie.isHttpOnly()).isTrue();
    }

    @Test
    void loginSessionLogoutCookie() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie userIdCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.SESSION_LOGOUT);
        this.userDao.refresh(user);
        assertThat(userIdCookie.getValue()).isEqualTo(user.getSessions().get(0).getSessionKey());
    }

    @Test
    void loginSessionLogoutCookieMaxAge() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.SESSION_LOGOUT);
        assertThat(sessionCookie.getMaxAge()).isEqualTo(this.testConfig.sessionLifeSeconds());
    }

    @Test
    void loginSessionLogoutCookiePath() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.SESSION_LOGOUT);
        assertThat(sessionCookie.getPath())
                .isEqualTo(String.format("/%s/api/logout", this.testConfig.userDeployName()));
    }

    @Test
    void loginSessionLogoutCookieHttpOnly() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        NewCookie sessionCookie = this.loginClient.login(user, password).getCookies().get(CookieNames.SESSION_LOGOUT);
        assertThat(sessionCookie.isHttpOnly()).isTrue();
    }
}
