package com.gitlab.akluball.chessapp.user.itest.client;

import com.gitlab.akluball.chessapp.user.itest.model.User;

import javax.inject.Inject;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.util.Objects;

public class LoginClient {
    private final WebTarget target;

    @Inject
    LoginClient(ApiWrapper apiWrapper) {
        this.target = apiWrapper.target().path("login");
    }

    public Response login(User user, String password) {
        return this.target.path(user.getHandle())
                .request()
                .post(Objects.nonNull(password) ? Entity.json(password) : null);
    }
}
