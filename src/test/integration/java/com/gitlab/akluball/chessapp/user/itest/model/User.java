package com.gitlab.akluball.chessapp.user.itest.model;

import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.ws.rs.core.NewCookie;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ChessUser")
@Access(AccessType.FIELD)
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String handle;
    private String passwordSalt;
    private String passwordHash;
    private int rating;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Session> sessions = new ArrayList<>();

    public int getId() {
        return this.id;
    }

    public String getHandle() {
        return this.handle;
    }

    public int getRating() {
        return this.rating;
    }

    public List<Session> getSessions() {
        return sessions;
    }

    public Session getSession(NewCookie sessionCookie) {
        return this.sessions.stream()
                .filter(s -> s.isSessionKey(sessionCookie.getValue()))
                .findFirst().orElse(null);
    }

    public static class Builder {
        private final User user;
        private String prefix;
        private String handle;

        Builder() {
            this.user = new User();
        }

        public Builder prefix(String prefix) {
            this.prefix = prefix;
            return this;
        }

        public Builder handle(String handle) {
            this.handle = handle;
            return this;
        }

        public Builder password(String password) {
            this.user.passwordSalt = BCrypt.gensalt();
            this.user.passwordHash = BCrypt.hashpw(password, this.user.passwordSalt);
            return this;
        }

        public Builder rating(int rating) {
            this.user.rating = rating;
            return this;
        }

        public Builder firstName(String firstName) {
            this.user.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.user.lastName = lastName;
            return this;
        }

        public Builder joinEpochSeconds(long epochSeconds) {
            this.user.joinEpochSeconds = epochSeconds;
            return this;
        }

        public Builder session(Session session) {
            this.user.sessions.add(session);
            return this;
        }

        public User build() {
            String prefix = (Objects.nonNull(this.prefix)) ? this.prefix : "";
            this.user.handle = prefix + this.handle;
            return this.user;
        }
    }
}
