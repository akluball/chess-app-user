package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.TokenClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;
import java.time.Instant;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class TokenControllerIT {
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final TokenClient tokenClient;

    @Guicy
    TokenControllerIT(UniqueBuilders uniqueBuilders, UserDao userDao, TokenClient tokenClient) {
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.tokenClient = tokenClient;
    }

    @Test
    void getToken() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken(user, session);
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    void getTokenNoUserIdCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken(null, session);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getTokenNoSessionCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken(user, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getTokenBadUserIdValue() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken("bad" + user.getId(), session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void getTokenUserNotFound() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        Response response = this.tokenClient.getToken(user, session);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getTokenBadSessionKey() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken(user, session);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void getTokenExpiredSession() {
        Session session = this.uniqueBuilders.session().expiration(Instant.now().minusSeconds(10)).build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.tokenClient.getToken(user, session);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }
}
