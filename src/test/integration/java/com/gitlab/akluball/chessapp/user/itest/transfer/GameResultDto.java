package com.gitlab.akluball.chessapp.user.itest.transfer;

import com.gitlab.akluball.chessapp.user.itest.model.User;

public class GameResultDto {
    private Integer whiteSideUserId;
    private Integer blackSideUserId;
    private Integer whiteSideUserRatingAdjustment;
    private Integer blackSideUserRatingAdjustment;

    public static class Builder {
        private final GameResultDto gameResultDto;

        Builder() {
            this.gameResultDto = new GameResultDto();
        }

        public Builder whiteSideUserId(Integer userId) {
            this.gameResultDto.whiteSideUserId = userId;
            return this;
        }

        public Builder whiteSideUser(User whiteSideUser) {
            return this.whiteSideUserId(whiteSideUser.getId());
        }

        public Builder blackSideUserId(Integer userId) {
            this.gameResultDto.blackSideUserId = userId;
            return this;
        }

        public Builder blackSideUser(User blackSideUser) {
            return this.blackSideUserId(blackSideUser.getId());
        }

        public Builder whiteSideUserRatingAdjustment(Integer ratingAdjustment) {
            this.gameResultDto.whiteSideUserRatingAdjustment = ratingAdjustment;
            return this;
        }

        public Builder blackSideUserRatingAdjustment(Integer ratingAdjustment) {
            this.gameResultDto.blackSideUserRatingAdjustment = ratingAdjustment;
            return this;
        }

        public GameResultDto build() {
            return this.gameResultDto;
        }
    }
}
