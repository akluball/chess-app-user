package com.gitlab.akluball.chessapp.user.itest.security;

public enum ServiceName {
    GAMEHISTORY("gamehistory");

    private final String asString;

    ServiceName(String asString) {
        this.asString = asString;
    }

    public String asString() {
        return this.asString;
    }
}
