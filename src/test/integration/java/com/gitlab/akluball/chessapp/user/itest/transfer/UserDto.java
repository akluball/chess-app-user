package com.gitlab.akluball.chessapp.user.itest.transfer;

public class UserDto {
    private int id;
    private String handle;
    private int rating;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;

    public int getId() {
        return id;
    }

    public String getHandle() {
        return handle;
    }

    public int getRating() {
        return rating;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public long getJoinEpochSeconds() {
        return joinEpochSeconds;
    }
}
