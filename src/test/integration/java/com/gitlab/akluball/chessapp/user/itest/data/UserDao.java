package com.gitlab.akluball.chessapp.user.itest.data;

import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class UserDao {
    private final EntityManager entityManager;

    @Inject
    UserDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(User... users) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        for (User user : users) {
            this.entityManager.persist(user);
        }
        tx.commit();
    }

    public void delete(User user) {
        EntityTransaction tx = this.entityManager.getTransaction();
        tx.begin();
        this.entityManager.remove(user);
        tx.commit();
    }

    public void refresh(User user) {
        this.entityManager.refresh(user);
    }

    public void refresh(Session session) {
        this.entityManager.refresh(session);
    }
}
