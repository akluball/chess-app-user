package com.gitlab.akluball.chessapp.user.itest.unique;

import com.gitlab.akluball.chessapp.user.itest.data.UniqueDataDao;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicInteger;

public class UniqueData {
    private static final String PREFIX_TEMPLATE = "pr%sef%six";
    private static final String HANDLE_TEMPLATE = "han%sdle%s";
    private static final String SESSION_KEY_TEMPLATE = "session%skey%s";
    private static final String PASSWORD_TEMPLATE = "pass%sword%s";

    private final int id;
    private final AtomicInteger prefixCounter;
    private final AtomicInteger handleCounter;
    private final AtomicInteger sessionKeyCounter;
    private final AtomicInteger passwordCounter;

    @Inject
    UniqueData(UniqueDataDao uniqueDataDao) {
        this.id = uniqueDataDao.nextId();
        this.prefixCounter = new AtomicInteger();
        this.handleCounter = new AtomicInteger();
        this.sessionKeyCounter = new AtomicInteger();
        this.passwordCounter = new AtomicInteger();
    }

    private String generate(String template, AtomicInteger counter) {
        return String.format(template, this.id, counter.getAndIncrement());
    }

    public String prefix() {
        return this.generate(PREFIX_TEMPLATE, this.prefixCounter);
    }

    public String handle() {
        return this.generate(HANDLE_TEMPLATE, this.handleCounter);
    }

    public String sessionKey() {
        return this.generate(SESSION_KEY_TEMPLATE, this.sessionKeyCounter);
    }

    public String password() {
        return this.generate(PASSWORD_TEMPLATE, this.passwordCounter);
    }
}
