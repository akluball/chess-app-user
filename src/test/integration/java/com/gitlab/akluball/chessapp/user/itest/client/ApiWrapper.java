package com.gitlab.akluball.chessapp.user.itest.client;

import com.gitlab.akluball.chessapp.user.itest.TestConfig;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

public class ApiWrapper {
    private final Client client;
    private String userApiUri;

    @Inject
    ApiWrapper(TestConfig testConfig, Client client) {
        this.client = client;
        this.userApiUri = String.format("%s/api", testConfig.userUri());
    }

    public WebTarget target() {
        return this.client.target(this.userApiUri);
    }
}
