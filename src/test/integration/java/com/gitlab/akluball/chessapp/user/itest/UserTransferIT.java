package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.UserClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.user.itest.transfer.UserDto;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserTransferIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final UserSecurity userSecurity;
    private final UserClient userClient;

    @Guicy
    UserTransferIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            UserDao userDao,
            UserSecurity userSecurity,
            UserClient userClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.userSecurity = userSecurity;
        this.userClient = userClient;
    }

    @Test
    void id() {
        User user = this.uniqueBuilders.user().build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getId()).isEqualTo(user.getId());
    }

    @Test
    void handle() {
        String handle = this.uniqueData.handle();
        User user = this.uniqueBuilders.user().handle(handle).build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getHandle()).isEqualTo(handle);
    }

    @Test
    void rating() {
        User user = this.uniqueBuilders.user().rating(800).build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getRating()).isEqualTo(800);
    }

    @Test
    void firstName() {
        User user = this.uniqueBuilders.user().firstName("bob").build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getFirstName()).isEqualTo("bob");
    }

    @Test
    void lastName() {
        User user = this.uniqueBuilders.user().lastName("smith").build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getLastName()).isEqualTo("smith");
    }

    @Test
    void joinEpochSeconds() {
        User user = this.uniqueBuilders.user().joinEpochSeconds(1111L).build();
        this.userDao.persist(user);
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), user);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getJoinEpochSeconds()).isEqualTo(1111L);
    }
}
