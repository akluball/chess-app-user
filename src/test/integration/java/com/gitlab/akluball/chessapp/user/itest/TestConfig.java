package com.gitlab.akluball.chessapp.user.itest;

import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.user.itest.hook.DeployUtil.*;

@Singleton
public class TestConfig {
    public static final String USER_PRIVATEKEY_PROP = "chessapp.user.privatekey";
    public static final String USER_PUBLICKEY_PROP = "chessapp.user.publickey";
    public static final String GAMEHISTORY_PRIVATEKEY_PROP = "chessapp.gamehistory.privatekey";

    public static final int SESSION_LIFE_SECONDS = 600;
    public static final int TOKEN_LIFE_SECONDS = 120;

    private final String userUri;
    private final String dbUri;
    private final String dbUser;
    private final String dbPassword;
    private final String userDeployName;
    private final PrivateKey userPrivateKey;
    private final RSAPublicKey userPublicKey;
    private final PrivateKey gameHistoryPrivateKey;

    TestConfig() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        this.userUri = getUserUri();
        this.dbUri = getDbUri();
        this.dbUser = DB_USER;
        this.dbPassword = DB_PASSWORD;
        try (InputStream configStream = Thread.currentThread()
                .getContextClassLoader()
                .getResourceAsStream("user-test.properties")) {
            Properties config = new Properties();
            config.load(configStream);
            this.userDeployName = USER_DEPLOY_NAME;
            Base64.Decoder decoder = Base64.getDecoder();
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            byte[] userPrivateKeyBytes = decoder.decode(config.getProperty(USER_PRIVATEKEY_PROP));
            this.userPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(userPrivateKeyBytes));
            byte[] userPublicKeyBytes = decoder.decode(config.getProperty(USER_PUBLICKEY_PROP));
            this.userPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(userPublicKeyBytes));
            byte[] gameHistoryPrivateKeyBytes = decoder.decode(config.getProperty(GAMEHISTORY_PRIVATEKEY_PROP));
            this.gameHistoryPrivateKey = keyFactory
                    .generatePrivate(new PKCS8EncodedKeySpec(gameHistoryPrivateKeyBytes));
        }
    }

    public String userUri() {
        return this.userUri;
    }

    public String dbUri() {
        return this.dbUri;
    }

    public String dbUser() {
        return this.dbUser;
    }

    public String dbPassword() {
        return this.dbPassword;
    }

    public String userDeployName() {
        return this.userDeployName;
    }

    public PrivateKey userPrivateKey() {
        return this.userPrivateKey;
    }

    public RSAPublicKey userPublicKey() {
        return this.userPublicKey;
    }

    public PrivateKey gameHistoryPrivateKey() {
        return this.gameHistoryPrivateKey;
    }

    public int sessionLifeSeconds() {
        return SESSION_LIFE_SECONDS;
    }

    public int tokenLifeSeconds() {
        return TOKEN_LIFE_SECONDS;
    }
}
