package com.gitlab.akluball.chessapp.user.itest.security;

import com.gitlab.akluball.chessapp.user.itest.TestConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.user.itest.util.Util.asRuntime;

@Singleton
public class GameHistorySecurity {

    private final String token;
    private final String expiredToken;

    @Inject
    GameHistorySecurity(TestConfig testConfig) {
        RSASSASigner signer = new RSASSASigner(testConfig.gameHistoryPrivateKey());
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(Instant.now().plusSeconds(60)))
                .build();
        JWTClaimsSet expiredClaimsSet = new JWTClaimsSet.Builder()
                .subject(ServiceName.GAMEHISTORY.asString())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.SERVICE.asString()))
                .expirationTime(Date.from(Instant.now().minusSeconds(60)))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        SignedJWT expiredToken = new SignedJWT(header, expiredClaimsSet);
        try {
            token.sign(signer);
            expiredToken.sign(signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        this.token = token.serialize();
        this.expiredToken = expiredToken.serialize();
    }

    public String getToken() {
        return this.token;
    }

    public String getExpiredToken() {
        return this.expiredToken;
    }
}
