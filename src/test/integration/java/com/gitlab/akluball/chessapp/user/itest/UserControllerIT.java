package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.UserClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.security.GameHistorySecurity;
import com.gitlab.akluball.chessapp.user.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.user.itest.transfer.GameResultDto;
import com.gitlab.akluball.chessapp.user.itest.transfer.UserDto;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import com.gitlab.akluball.chessapp.user.itest.util.Builders;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class UserControllerIT {
    private final Builders builders;
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final UserSecurity userSecurity;
    private final GameHistorySecurity gameHistorySecurity;
    private final UserClient userClient;

    @Guicy
    UserControllerIT(Builders builders,
            UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            UserDao userDao,
            UserSecurity userSecurity,
            GameHistorySecurity gameHistorySecurity,
            UserClient userClient) {
        this.builders = builders;
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.userSecurity = userSecurity;
        this.gameHistorySecurity = gameHistorySecurity;
        this.userClient = userClient;
    }

    private User createPersistedUser() {
        User user = this.uniqueBuilders.user().build();
        this.userDao.persist(user);
        return user;
    }

    private User createUnpersistedUser() {
        User user = this.uniqueBuilders.user().build();
        // this is so that user gets generated id
        this.userDao.persist(user);
        this.userDao.delete(user);
        return user;
    }

    @Test
    void currentUser() {
        User user = this.createPersistedUser();
        Response response = this.userClient.getCurrentUser(this.userSecurity.tokenFor(user));
        assertThat(response.getStatus()).isEqualTo(200);
        UserDto currentUser = response.readEntity(UserDto.class);
        assertThat(currentUser.getId()).isEqualTo(user.getId());
    }

    @Test
    void currentUserNoAuth() {
        Response unpersistedUser = this.userClient.getCurrentUser(null);
        assertThat(unpersistedUser.getStatus()).isEqualTo(401);
        assertThat(unpersistedUser.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void currentUserExpiredToken() {
        String token = this.userSecurity.expiredTokenFor(this.createPersistedUser());
        Response response = this.userClient.getCurrentUser(token);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void currentUserNotFound() {
        User userId = this.uniqueBuilders.user().build();
        Response response = this.userClient.getCurrentUser(this.userSecurity.tokenFor(userId));
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void findByIdFromUser() {
        User requester = this.createPersistedUser();
        User toFind = this.createPersistedUser();
        Response response = this.userClient.findById(this.userSecurity.tokenFor(requester), toFind);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getId()).isEqualTo(toFind.getId());
    }

    @Test
    void findByIdFromGameHistory() {
        User toFind = this.createPersistedUser();
        Response response = this.userClient.findById(this.gameHistorySecurity.getToken(), toFind);
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.readEntity(UserDto.class).getId()).isEqualTo(toFind.getId());
    }

    @Test
    void findByIdNoAuth() {
        Response response = this.userClient.findById(null, this.createPersistedUser());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void findByIdNotFound() {
        User user = this.createPersistedUser();
        User toFind = this.uniqueBuilders.user().build();
        Response response = this.userClient.findById(this.userSecurity.tokenFor(user), toFind);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void findByIdNotInteger() {
        User requester = this.createPersistedUser();
        Response response = this.userClient.findById(this.userSecurity.tokenFor(requester), "not-an-integer");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void searchCaseInsensitive() {
        User currentUser = this.createPersistedUser();
        String prefix = this.uniqueData.prefix();
        User user = this.uniqueBuilders.user().prefix(prefix).build();
        this.userDao.persist(user);
        Response response = this.userClient.search(this.userSecurity.tokenFor(currentUser), prefix.toUpperCase());
        assertThat(response.getStatus()).isEqualTo(200);
        List<User> users = response.readEntity(new GenericType<List<User>>() {});
        assertThat(users).hasSize(1);
        assertThat(users.get(0).getHandle()).isEqualTo(user.getHandle());
    }

    @Test
    void searchOrdersByHandle() {
        User currentUser = this.createPersistedUser();
        String prefix = this.uniqueData.prefix();
        String handle = this.uniqueData.handle();
        User first = this.uniqueBuilders.user().prefix(prefix).handle(handle + "a").build();
        User second = this.uniqueBuilders.user().prefix(prefix).handle(handle + "b").build();
        User third = this.uniqueBuilders.user().prefix(prefix).handle(handle + "c").build();
        this.userDao.persist(first, second, third);
        Response response = this.userClient.search(this.userSecurity.tokenFor(currentUser), prefix.toUpperCase());
        assertThat(response.getStatus()).isEqualTo(200);
        List<User> users = response.readEntity(new GenericType<List<User>>() {});
        assertThat(users).hasSize(3);
        assertThat(users.get(0).getHandle()).isEqualTo(first.getHandle());
        assertThat(users.get(1).getHandle()).isEqualTo(second.getHandle());
        assertThat(users.get(2).getHandle()).isEqualTo(third.getHandle());
    }

    @Test
    void searchNoAuth() {
        Response response = this.userClient.search(null, "abc");
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void searchNoSearchString() {
        User user = this.createPersistedUser();
        Response response = this.userClient.search(this.userSecurity.tokenFor(user), null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void whitespaceSearchString() {
        User user = this.createPersistedUser();
        Response response = this.userClient.search(this.userSecurity.tokenFor(user), "\n");
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGame() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void endGameNoAuth() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(null, gameResultDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void endGameExpiredToken() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .blackSideUser(this.createPersistedUser()).whiteSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getExpiredToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void endGameNoBody() {
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), null);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoWhiteSideUserId() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUserId(null).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoBlackSideUserId() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUserId(null)
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoWhiteSideUserRating() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(null).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameNoBlackSideUserRating() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(null).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void endGameWhiteSideUserNotFound() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createUnpersistedUser()).blackSideUser(this.createPersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }

    @Test
    void endGameBlackSideUserNotFound() {
        GameResultDto gameResultDto = this.builders.gameResultDto()
                .whiteSideUser(this.createPersistedUser()).blackSideUser(this.createUnpersistedUser())
                .whiteSideUserRatingAdjustment(0).blackSideUserRatingAdjustment(0).build();
        Response response = this.userClient.endGame(this.gameHistorySecurity.getToken(), gameResultDto);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }
}
