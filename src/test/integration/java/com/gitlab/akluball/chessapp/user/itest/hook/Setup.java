package com.gitlab.akluball.chessapp.user.itest.hook;

import com.gitlab.akluball.chessapp.user.itest.TestConfig;

import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Properties;

import static com.gitlab.akluball.chessapp.user.itest.TestConfig.*;
import static com.gitlab.akluball.chessapp.user.itest.hook.DeployUtil.*;
import static com.gitlab.akluball.chessapp.user.itest.util.Util.sleepMillis;

public class Setup {
    public static void main(String[] args) throws IOException, InterruptedException, NoSuchAlgorithmException {
        // keys
        Base64.Encoder encoder = Base64.getEncoder();
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048);
        // user service keys
        KeyPair userKeyPair = keyPairGenerator.generateKeyPair();
        String userPrivateKey = new String(encoder.encode(userKeyPair.getPrivate().getEncoded()));
        String userPublicKey = new String(encoder.encode(userKeyPair.getPublic().getEncoded()));
        // gamehistory service keys
        KeyPair gameHistoryKeyPair = keyPairGenerator.generateKeyPair();
        String gameHistoryPrivateKey = new String(encoder.encode(gameHistoryKeyPair.getPrivate().getEncoded()));
        String gameHistoryPublicKey = new String(encoder.encode(gameHistoryKeyPair.getPublic().getEncoded()));
        // save keys for test runs
        Properties testProps = new Properties();
        testProps.put(USER_PRIVATEKEY_PROP, userPrivateKey);
        testProps.put(USER_PUBLICKEY_PROP, userPublicKey);
        testProps.put(GAMEHISTORY_PRIVATEKEY_PROP, gameHistoryPrivateKey);
        FileWriter testPropsWriter = new FileWriter(System.getenv("USER_TEST_PROPERTIES"));
        testProps.store(testPropsWriter, null);
        // containers
        new ProcessBuilder("docker", "network", "create", NETWORK_NAME)
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", DB_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("POSTGRES_DB=%s", DB_NAME), "--env", String.format("POSTGRES_USER=%s", DB_USER),
                "--env", String.format("POSTGRES_PASSWORD=%s", DB_PASSWORD), "--rm", "--detach",
                POSTGRES_IMAGE,
                "-c", "max_connections=200")
                .inheritIO().start().waitFor();
        new ProcessBuilder("docker", "run", "--name", PAYARA_CONTAINER, "--network", NETWORK_NAME,
                "--env", String.format("DB_SERVER=%s", getDbIp()),
                "--env", String.format("DB_NAME=%s", DB_NAME),
                "--env", String.format("DB_USER=%s", DB_USER),
                "--env", String.format("DB_PASSWORD=%s", DB_PASSWORD),
                "--env", String.format("CHESSAPP_USER_PRIVATEKEY=%s", userPrivateKey),
                "--env", String.format("CHESSAPP_USER_PUBLICKEY=%s", userPublicKey),
                "--env", String.format("CHESSAPP_GAMEHISTORY_PUBLICKEY=%s", gameHistoryPublicKey),
                "--env", String.format("CHESSAPP_USER_SESSIONLIFESECONDS=%s", TestConfig.SESSION_LIFE_SECONDS),
                "--env", String.format("CHESSAPP_USER_TOKENLIFESECONDS=%s", TestConfig.TOKEN_LIFE_SECONDS),
                "--rm", "--detach", PAYARA_IMAGE)
                .inheritIO().start().waitFor();
        String userWar = System.getenv("CHESSAPP_USER_WAR");
        copyToContainer(userWar, PAYARA_CONTAINER, CONTAINER_USER_WAR);
        // wait for payara to come up
        int uptimeAttempts = 6;
        while (uptimeAttempts > 0 && !containerAsadmin("uptime")) {
            sleepMillis(500);
            uptimeAttempts--;
        }
        containerAsadmin("deploy", "--name", USER_DEPLOY_NAME, CONTAINER_USER_WAR);
    }
}
