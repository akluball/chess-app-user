package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.TokenClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.security.BearerRole;
import com.gitlab.akluball.chessapp.user.itest.security.UserSecurity;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.nimbusds.jwt.SignedJWT;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.time.Instant;

import static com.gitlab.akluball.chessapp.user.itest.security.SecurityUtil.parseSignedToken;
import static com.gitlab.akluball.chessapp.user.itest.util.Util.buffer5Seconds;
import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class TokenTransferIT {
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final TokenClient tokenClient;
    private final UserSecurity userSecurity;

    @Guicy
    TokenTransferIT(UniqueBuilders uniqueBuilders,
            UserDao userDao,
            TokenClient tokenClient,
            UserSecurity userSecurity) {
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.tokenClient = tokenClient;
        this.userSecurity = userSecurity;
    }

    @Test
    void tokenValidSignature() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        SignedJWT token = parseSignedToken(this.tokenClient.getToken(user, session).readEntity(String.class));
        assertThat(this.userSecurity.isValidSignature(token)).isTrue();
    }

    @Test
    void tokenUserId() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        SignedJWT token = parseSignedToken(this.tokenClient.getToken(user, session).readEntity(String.class));
        assertThat(this.userSecurity.getUserId(token)).isEqualTo(user.getId());
    }

    @Test
    void tokenRole() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        SignedJWT token = parseSignedToken(this.tokenClient.getToken(user, session).readEntity(String.class));
        assertThat(this.userSecurity.getRoles(token)).containsExactly(BearerRole.USER);
    }

    @Test
    @Guicy
    void tokenExpiration(TestConfig testConfig) {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        SignedJWT token = parseSignedToken(this.tokenClient.getToken(user, session).readEntity(String.class));
        Instant expirationRange = Instant.now().plusSeconds(testConfig.tokenLifeSeconds());
        assertThat(this.userSecurity.getExpiration(token)).isIn(buffer5Seconds(expirationRange));
    }
}
