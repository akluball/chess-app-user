package com.gitlab.akluball.chessapp.user.itest.util;

import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.transfer.GameResultDto;
import com.google.inject.Inject;

import javax.inject.Provider;

public class Builders {
    private final Provider<User.Builder> userBuilderProvider;
    private final Provider<Session.Builder> sessionBuilderProvider;
    private final Provider<GameResultDto.Builder> gameResultDtoBuilderProvider;

    @Inject
    Builders(Provider<User.Builder> userBuilderProvider,
            Provider<Session.Builder> sessionBuilderProvider,
            Provider<GameResultDto.Builder> gameResultDtoBuilderProvider) {
        this.userBuilderProvider = userBuilderProvider;
        this.sessionBuilderProvider = sessionBuilderProvider;
        this.gameResultDtoBuilderProvider = gameResultDtoBuilderProvider;
    }

    public User.Builder user() {
        return this.userBuilderProvider.get();
    }

    public Session.Builder session() {
        return this.sessionBuilderProvider.get();
    }

    public GameResultDto.Builder gameResultDto() {
        return this.gameResultDtoBuilderProvider.get();
    }
}
