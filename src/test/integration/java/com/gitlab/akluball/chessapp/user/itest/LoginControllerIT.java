package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.LoginClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class LoginControllerIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final LoginClient loginClient;

    @Guicy
    LoginControllerIT(UniqueData uniqueData, UniqueBuilders uniqueBuilders, UserDao userDao, LoginClient loginClient) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.loginClient = loginClient;
    }

    @Test
    void login() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user, password);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void loginBadIdentifier() {
        String password = this.uniqueData.password();
        User user = this.uniqueBuilders.user().password(password).build();
        Response response = this.loginClient.login(user, password);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void loginNoPassword() {
        User user = this.uniqueBuilders.user().password(this.uniqueData.password()).build();
        Response response = this.loginClient.login(user, null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void loginEmptyPassword() {
        User user = this.uniqueBuilders.user().password(this.uniqueData.password()).build();
        Response response = this.loginClient.login(user, "");
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void loginBadPassword() {
        User user = this.uniqueBuilders.user().password(this.uniqueData.password()).build();
        this.userDao.persist(user);
        Response response = this.loginClient.login(user, this.uniqueData.password());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }
}
