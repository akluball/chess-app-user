package com.gitlab.akluball.chessapp.user.itest.security;

public class CookieNames {
    public static final String USER_ID_TOKEN = "chessapp-user-id-token";
    public static final String SESSION_TOKEN = "chessapp-user-session-token";
    public static final String USER_ID_LOGOUT = "chessapp-user-id-logout";
    public static final String SESSION_LOGOUT = "chessapp-user-session-logout";
}
