package com.gitlab.akluball.chessapp.user.itest;

import com.gitlab.akluball.chessapp.user.itest.client.LogoutClient;
import com.gitlab.akluball.chessapp.user.itest.data.UserDao;
import com.gitlab.akluball.chessapp.user.itest.model.Session;
import com.gitlab.akluball.chessapp.user.itest.model.User;
import com.gitlab.akluball.chessapp.user.itest.param.Guicy;
import com.gitlab.akluball.chessapp.user.itest.param.GuicyParameterResolver;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueBuilders;
import com.gitlab.akluball.chessapp.user.itest.unique.UniqueData;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.ws.rs.core.Response;

import static com.google.common.truth.Truth.assertThat;

@ExtendWith(GuicyParameterResolver.class)
public class LogoutControllerIT {
    private final UniqueData uniqueData;
    private final UniqueBuilders uniqueBuilders;
    private final UserDao userDao;
    private final TestConfig testConfig;
    private final LogoutClient logoutClient;

    @Guicy
    LogoutControllerIT(UniqueData uniqueData,
            UniqueBuilders uniqueBuilders,
            UserDao userDao,
            LogoutClient logoutClient,
            TestConfig testConfig) {
        this.uniqueData = uniqueData;
        this.uniqueBuilders = uniqueBuilders;
        this.userDao = userDao;
        this.logoutClient = logoutClient;
        this.testConfig = testConfig;
    }

    @Test
    void logout() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(user, session);
        assertThat(response.getStatus()).isEqualTo(204);
    }

    @Test
    void logoutNoUserIdCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout(null, session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void logoutNonIntegerUserIdCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout("not-an-integer", session.getSessionKey());
        assertThat(response.getStatus()).isEqualTo(400);
        assertThat(response.readEntity(String.class)).containsMatch("bad request");
    }

    @Test
    void logoutNoSessionCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout("" + user.getId(), null);
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void logoutEmptySessionCookie() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        Response response = this.logoutClient.logout("" + user.getId(), "");
        assertThat(response.getStatus()).isEqualTo(401);
        assertThat(response.readEntity(String.class)).containsMatch("invalid credentials");
    }

    @Test
    void logoutUserNotFound() {
        Session session = this.uniqueBuilders.session().build();
        User user = this.uniqueBuilders.user().session(session).build();
        this.userDao.persist(user);
        this.userDao.delete(user);
        Response response = this.logoutClient.logout(user, session);
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.readEntity(String.class)).containsMatch("not found");
    }
}
