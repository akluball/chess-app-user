package com.gitlab.akluball.chessapp.user.itest.hook;

import com.gitlab.akluball.chessapp.user.itest.param.ParameterResolverModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import static com.gitlab.akluball.chessapp.user.itest.hook.DeployUtil.*;

public class Before {
    public static void main(String[] args) {
        String userWar = System.getenv("CHESSAPP_USER_WAR");
        copyToContainer(userWar, PAYARA_CONTAINER, CONTAINER_USER_WAR);
        containerAsadmin("redeploy", "--name", USER_DEPLOY_NAME, CONTAINER_USER_WAR);
        Injector injector = Guice.createInjector(new ParameterResolverModule());
        EntityManager entityManager = injector.getInstance(EntityManager.class);
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.flush();
        tx.commit();
    }
}
