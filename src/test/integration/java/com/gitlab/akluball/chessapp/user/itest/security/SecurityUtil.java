package com.gitlab.akluball.chessapp.user.itest.security;

import com.nimbusds.jwt.SignedJWT;

import javax.ws.rs.client.Invocation;
import java.text.ParseException;

import static com.gitlab.akluball.chessapp.user.itest.util.Util.asRuntime;

public class SecurityUtil {
    public static void bearer(Invocation.Builder invocationBuilder, String token) {
        if (token != null) {
            invocationBuilder.header("Authorization", String.format("Bearer %s", token));
        }
    }

    public static SignedJWT parseSignedToken(String serializedToken) {
        try {
            return SignedJWT.parse(serializedToken);
        } catch (ParseException e) {
            throw asRuntime(e);
        }
    }
}
