package com.gitlab.akluball.chessapp.user.model;

import javax.enterprise.context.Dependent;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Entity
@Access(AccessType.FIELD)
public class Session {
    @Id
    @GeneratedValue
    private int id;
    private String sessionKey;
    @AttributeOverride(name = "epochSecond", column = @Column(name = "expEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "expNano"))
    private PersistableInstant expiration;
    @AttributeOverride(name = "epochSecond", column = @Column(name = "tokenExpEpochSecond"))
    @AttributeOverride(name = "nanoComponent", column = @Column(name = "tokenExpNano"))
    private PersistableInstant freshestTokenExpiration;

    public boolean isNotExpired() {
        return this.expiration.toInstant().isAfter(Instant.now());
    }

    public boolean isSessionKey(String sessionKey) {
        return this.sessionKey.equals(sessionKey);
    }

    public void freshestTokenExpiresAt(Instant freshestTokenExpiration) {
        this.freshestTokenExpiration = PersistableInstant.from(freshestTokenExpiration);
    }

    @Dependent
    public static class Builder {
        private final Session session;

        public Builder() {
            this.session = new Session();
            this.session.freshestTokenExpiration = PersistableInstant.now();
        }

        public Builder sessionKey(String sessionKey) {
            this.session.sessionKey = sessionKey;
            return this;
        }

        public Builder secondsToExpiration(int secondsToExpiration) {
            this.session.expiration = PersistableInstant.from(Instant.now().plusSeconds(secondsToExpiration));
            return this;
        }

        public Session build() {
            return this.session;
        }
    }
}
