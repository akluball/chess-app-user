package com.gitlab.akluball.chessapp.user.controller;

import com.gitlab.akluball.chessapp.user.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.user.security.CookieNames;
import com.gitlab.akluball.chessapp.user.service.TokenService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("token")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.AUTH)
public class TokenController {
    private TokenService tokenService;

    public TokenController() {
    }

    @Inject
    public TokenController(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @GET
    @Operation(summary = "Gets auth token", description = "Gets auth token for user identified by cookies")
    @ApiResponse(
            responseCode = "200",
            description = "Token successfully fetched",
            content = @Content(
                    schema = @Schema(description = "The auth token", example = "abc.123")
            )
    )
    @ApiResponse(responseCode = "400", description = "bad request")
    @ApiResponse(responseCode = "401", description = "invalid credentials")
    public String getToken(
            @CookieParam(CookieNames.USER_ID_TOKEN) Integer unparsedUserId,
            @CookieParam(CookieNames.SESSION_TOKEN) String sessionKey
    ) {
        return this.tokenService.createToken(unparsedUserId, sessionKey);
    }
}
