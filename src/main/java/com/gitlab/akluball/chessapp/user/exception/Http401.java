package com.gitlab.akluball.chessapp.user.exception;

public class Http401 extends HttpException {
    public Http401() {
        super(401, "invalid credentials");
    }

    public static class Payload implements javax.validation.Payload {}
}
