package com.gitlab.akluball.chessapp.user.security;

import com.gitlab.akluball.chessapp.user.UserConfig;
import com.gitlab.akluball.chessapp.user.model.User;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;

import static com.gitlab.akluball.chessapp.user.util.Util.asRuntime;

@Singleton
public class UserSecurity {
    private RSASSASigner signer;
    private RSASSAVerifier verifier;

    @Inject
    UserSecurity(UserConfig userConfig) {
        this.signer = new RSASSASigner(userConfig.userPrivateKey());
        this.verifier = new RSASSAVerifier(userConfig.userPublicKey());
    }

    public String createToken(User user, Instant expiration) {
        JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).build();
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
                .subject("" + user.getId())
                .claim(BearerRole.CLAIM_NAME, Collections.singletonList(BearerRole.USER.asString()))
                .expirationTime(Date.from(expiration))
                .build();
        SignedJWT token = new SignedJWT(header, claimsSet);
        try {
            token.sign(this.signer);
        } catch (JOSEException e) {
            throw asRuntime(e);
        }
        return token.serialize();
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}
