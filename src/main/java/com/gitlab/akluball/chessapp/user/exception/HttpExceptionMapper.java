package com.gitlab.akluball.chessapp.user.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class HttpExceptionMapper implements ExceptionMapper<HttpException> {
    @Override
    public Response toResponse(HttpException httpException) {
        return Response.status(httpException.getStatus())
                .entity(httpException.getMessage())
                .build();
    }
}
