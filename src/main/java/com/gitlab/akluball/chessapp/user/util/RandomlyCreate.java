package com.gitlab.akluball.chessapp.user.util;

import javax.inject.Singleton;
import java.util.Random;

@Singleton
public class RandomlyCreate {
    private static final char[] ALPHANUM_ALPHABET = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
            'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2',
            '3', '4', '5', '6', '7', '8', '9' };
    private final Random random;

    RandomlyCreate() {
        this.random = new Random();
    }

    public String sessionKey() {
        String sessionKey = "";
        for (int i = 0; i < 15; i++) {
            sessionKey += ALPHANUM_ALPHABET[this.random.nextInt(ALPHANUM_ALPHABET.length)];
        }
        return sessionKey;
    }
}
