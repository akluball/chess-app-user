package com.gitlab.akluball.chessapp.user.controller.openapi;

public class OpenApiTags {
    public static final String AUTH = "Auth";
    public static final String USER = "User";
}
