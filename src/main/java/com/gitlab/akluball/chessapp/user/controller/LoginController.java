package com.gitlab.akluball.chessapp.user.controller;

import com.gitlab.akluball.chessapp.user.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.user.security.CookieFromScratch;
import com.gitlab.akluball.chessapp.user.security.CookieNames;
import com.gitlab.akluball.chessapp.user.service.LoginService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.stream.Stream;

@Path("login")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.AUTH)
public class LoginController {
    private LoginService loginService;

    public LoginController() {
    }

    @Inject
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @POST
    @Path("{identifier}")
    @Operation(summary = "Logs in user", description = "Logs in user identified by identifier (handle)")
    @ApiResponse(
            responseCode = "204",
            description = "Log in success",
            headers = {
                    @Header(
                            name = "Set-Cookie[0]",
                            description = "user id cookie",
                            schema = @Schema(example = CookieNames.USER_ID_TOKEN + "=1..."),
                            required = true
                    ),
                    @Header(
                            name = "Set-Cookie[1]",
                            description = "session key cookie",
                            schema = @Schema(example = CookieNames.SESSION_TOKEN + "=abc123..."),
                            required = true
                    )
            }
    )
    @ApiResponse(responseCode = "401", description = "Invalid credentials")
    public Response login(@PathParam("identifier") String identifier, String password) {
        CookieFromScratch[] cookies = this.loginService.login(identifier, password);
        Response.ResponseBuilder responseBuilder = Response.noContent();
        Stream.of(cookies).map(CookieFromScratch::headerValue)
                .forEach(hv -> responseBuilder.header("Set-Cookie", hv));
        return responseBuilder.build();
    }
}
