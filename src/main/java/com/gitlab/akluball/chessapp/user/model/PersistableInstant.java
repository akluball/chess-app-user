package com.gitlab.akluball.chessapp.user.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import java.time.Instant;

@Embeddable
@Access(AccessType.FIELD)
public class PersistableInstant {
    private long epochSecond;
    private int nanoComponent;

    static PersistableInstant now() {
        PersistableInstant persistableInstant = new PersistableInstant();
        Instant now = Instant.now();
        persistableInstant.epochSecond = now.getEpochSecond();
        persistableInstant.nanoComponent = now.getNano();
        return persistableInstant;
    }

    static PersistableInstant from(Instant instant) {
        PersistableInstant persistableInstant = new PersistableInstant();
        persistableInstant.epochSecond = instant.getEpochSecond();
        persistableInstant.nanoComponent = instant.getNano();
        return persistableInstant;
    }

    Instant toInstant() {
        return Instant.ofEpochSecond(this.epochSecond, this.nanoComponent);
    }
}
