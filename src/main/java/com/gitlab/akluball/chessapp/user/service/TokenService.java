package com.gitlab.akluball.chessapp.user.service;

import com.gitlab.akluball.chessapp.user.UserConfig;
import com.gitlab.akluball.chessapp.user.data.UserDao;
import com.gitlab.akluball.chessapp.user.exception.Http401;
import com.gitlab.akluball.chessapp.user.model.User;
import com.gitlab.akluball.chessapp.user.security.UserSecurity;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Objects;

@Stateless
public class TokenService {
    private UserDao userDao;
    private UserSecurity userSecurity;
    private int tokenLifeSeconds;

    public TokenService() {
    }

    @Inject
    public TokenService(UserDao userDao, UserSecurity userSecurity, UserConfig userConfig) {
        this.userDao = userDao;
        this.userSecurity = userSecurity;
        this.tokenLifeSeconds = userConfig.tokenLifeSeconds();
    }

    public String createToken(
            @NotNull(payload = Http401.Payload.class) Integer currentUserId,
            @NotBlank(payload = Http401.Payload.class) String sessionKey
    ) {
        User user = this.userDao.findById(currentUserId);
        if (Objects.isNull(user)) {
            throw new Http401();
        }
        if (!user.isValidSessionKey(sessionKey)) {
            throw new Http401();
        }
        Instant expiration = Instant.now().plusSeconds(this.tokenLifeSeconds);
        user.updateSession(sessionKey, expiration);
        return this.userSecurity.createToken(user, expiration);
    }
}
