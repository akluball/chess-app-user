package com.gitlab.akluball.chessapp.user.data;

import com.gitlab.akluball.chessapp.user.model.User;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class UserDao {

    @PersistenceContext(unitName = "chessUserPu")
    private EntityManager entityManager;

    public User findById(int userId) {
        return this.entityManager.find(User.class, userId);
    }

    public User findByIdentifier(String identifier) {
        TypedQuery<User> query = this.entityManager.createNamedQuery(User.Query.Name.FIND_BY_HANDLE, User.class);
        query.setParameter(User.Query.Param.HANDLE, identifier);
        return query.getResultStream()
                .findFirst()
                .orElse(null);
    }

    public List<User> search(String searchString) {
        TypedQuery<User> searchQuery = this.entityManager.createNamedQuery(User.Query.Name.SEARCH, User.class);
        searchQuery.setParameter(User.Query.Param.SEARCH_STRING, searchString);
        return searchQuery.getResultList();
    }
}
