package com.gitlab.akluball.chessapp.user.controller;

import com.gitlab.akluball.chessapp.user.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.user.security.CookieNames;
import com.gitlab.akluball.chessapp.user.service.LogoutService;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("logout")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.AUTH)
public class LogoutController {
    private LogoutService logoutService;

    public LogoutController() {
    }

    @Inject
    public LogoutController(LogoutService logoutService) {
        this.logoutService = logoutService;
    }

    @POST
    public void logout(
            @CookieParam(CookieNames.USER_ID_LOGOUT) Integer userId,
            @CookieParam(CookieNames.SESSION_LOGOUT) String sessionKey
    ) {
        this.logoutService.logout(userId, sessionKey);
    }
}
