package com.gitlab.akluball.chessapp.user.util;

public class Util {
    public static RuntimeException asRuntime(Throwable throwable) {
        return new RuntimeException(throwable);
    }

    public static int minutesToSeconds(int minutes) {
        return minutes * 60;
    }

    public static int hoursToSeconds(int hours) {
        return minutesToSeconds(60) * hours;
    }

    public static boolean isInteger(String toCheck) {
        try {
            Integer.parseInt(toCheck);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
