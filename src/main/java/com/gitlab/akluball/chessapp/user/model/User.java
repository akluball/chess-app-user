package com.gitlab.akluball.chessapp.user.model;

import com.gitlab.akluball.chessapp.user.exception.Http404;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.security.crypto.bcrypt.BCrypt;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "ChessUser")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = User.Query.Name.FIND_BY_HANDLE, query = User.Query.FIND_BY_HANDLE),
        @NamedQuery(name = User.Query.Name.SEARCH, query = User.Query.SEARCH)
})
public class User {
    @Id
    @GeneratedValue
    private int id;
    private String handle;
    private String passwordSalt;
    private String passwordHash;
    private int rating;
    private String firstName;
    private String lastName;
    private long joinEpochSeconds;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Session> sessions = new ArrayList<>();

    public int getId() {
        return this.id;
    }

    public boolean isPassword(String password) {
        return BCrypt.hashpw(password, this.passwordSalt).equals(this.passwordHash);
    }

    public void adjustRating(int adjustment) {
        this.rating += adjustment;
    }

    public void addSession(Session session) {
        this.sessions.add(session);
    }

    public boolean isValidSessionKey(String sessionKey) {
        this.sessions = this.sessions.stream()
                .filter(Session::isNotExpired)
                .collect(Collectors.toList());
        return this.sessions.stream()
                .anyMatch(session -> session.isSessionKey(sessionKey));
    }

    public void updateSession(String sessionKey, Instant freshestTokenExpiration) {
        Session session = this.sessions.stream()
                .filter(s -> s.isSessionKey(sessionKey))
                .findFirst().orElseThrow(() -> new Http404("Unknown session key"));
        session.freshestTokenExpiresAt(freshestTokenExpiration);
    }

    public void invalidateSession(String sessionKey) {
        this.sessions = this.sessions.stream()
                .filter(session -> !session.isSessionKey(sessionKey))
                .collect(Collectors.toList());
    }

    public Dto toDto() {
        return this.new Dto();
    }

    public static class Query {
        public static final String FIND_BY_HANDLE
                = "SELECT user FROM User AS user"
                + " WHERE user.handle=:" + Param.HANDLE;
        public static final String SEARCH
                = "SELECT user FROM User AS user"
                + " WHERE LOWER(user.handle) LIKE CONCAT('%', LOWER(:" + Param.SEARCH_STRING + "), '%')"
                + " ORDER BY LOWER(user.handle) ASC";

        public static class Param {
            public static final String HANDLE = "handle";
            public static final String SEARCH_STRING = "searchString";
        }

        public static class Name {
            public static final String FIND_BY_HANDLE = "find-by-handle";
            public static final String SEARCH = "search-users";
        }
    }

    @Schema(name = "User")
    public class Dto {
        private int id;
        private String handle;
        private int rating;
        private String firstName;
        private String lastName;
        private long joinEpochSeconds;

        private Dto() {
            User user = User.this;
            this.id = user.id;
            this.handle = user.handle;
            this.rating = user.rating;
            this.firstName = user.firstName;
            this.lastName = user.lastName;
            this.joinEpochSeconds = user.joinEpochSeconds;
        }
    }
}
