package com.gitlab.akluball.chessapp.user.security;

import com.nimbusds.jwt.SignedJWT;

import java.text.ParseException;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;

public class SecurityUtil {
    public static boolean isNotExpired(SignedJWT token) {
        try {
            Date expiration = token.getJWTClaimsSet().getExpirationTime();
            return Objects.nonNull(expiration) && expiration.toInstant().isAfter(Instant.now());
        } catch (ParseException e) {
            return false;
        }
    }
}
