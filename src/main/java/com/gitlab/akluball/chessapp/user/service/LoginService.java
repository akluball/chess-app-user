package com.gitlab.akluball.chessapp.user.service;

import com.gitlab.akluball.chessapp.user.UserConfig;
import com.gitlab.akluball.chessapp.user.data.UserDao;
import com.gitlab.akluball.chessapp.user.exception.Http401;
import com.gitlab.akluball.chessapp.user.model.Session;
import com.gitlab.akluball.chessapp.user.model.User;
import com.gitlab.akluball.chessapp.user.security.CookieFromScratch;
import com.gitlab.akluball.chessapp.user.security.CookieNames;
import com.gitlab.akluball.chessapp.user.util.RandomlyCreate;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Provider;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Stateless
public class LoginService {
    private UserDao userDao;
    private RandomlyCreate randomlyCreate;
    private Provider<CookieFromScratch.Builder> cookieBuilderProvider;
    private Provider<Session.Builder> sessionBuilderProvider;
    private int sessionLifeSeconds;

    public LoginService() {
    }

    @Inject
    public LoginService(UserDao userDao,
            RandomlyCreate randomlyCreate,
            Provider<CookieFromScratch.Builder> cookieBuilderProvider,
            Provider<Session.Builder> sessionBuilderProvider,
            UserConfig userConfig) {
        this.userDao = userDao;
        this.randomlyCreate = randomlyCreate;
        this.cookieBuilderProvider = cookieBuilderProvider;
        this.sessionBuilderProvider = sessionBuilderProvider;
        this.sessionLifeSeconds = userConfig.sessionLifeSeconds();
    }

    public CookieFromScratch[] login(
            @NotBlank(payload = Http401.Payload.class) String identifier,
            @NotBlank(payload = Http401.Payload.class) String password
    ) {
        User user = this.userDao.findByIdentifier(identifier);
        if (Objects.isNull(user) || !user.isPassword(password)) {
            throw new Http401();
        }
        String tokenCookiePath = "/api/token";
        String logoutCookiePath = "/api/logout";
        CookieFromScratch userIdTokenCookie = cookieBuilderProvider.get()
                .name(CookieNames.USER_ID_TOKEN)
                .value("" + user.getId())
                .path(tokenCookiePath)
                .maxAgeSeconds(this.sessionLifeSeconds)
                .build();
        CookieFromScratch userIdLogoutCookie = cookieBuilderProvider.get()
                .name(CookieNames.USER_ID_LOGOUT)
                .value("" + user.getId())
                .maxAgeSeconds(this.sessionLifeSeconds)
                .path(logoutCookiePath)
                .build();
        String sessionKey = this.randomlyCreate.sessionKey();
        CookieFromScratch sessionTokenCookie = cookieBuilderProvider.get()
                .name(CookieNames.SESSION_TOKEN)
                .value(sessionKey)
                .path(tokenCookiePath)
                .maxAgeSeconds(this.sessionLifeSeconds)
                .build();
        CookieFromScratch sessionLogoutCookie = cookieBuilderProvider.get()
                .name(CookieNames.SESSION_LOGOUT)
                .value(sessionKey)
                .maxAgeSeconds(this.sessionLifeSeconds)
                .path(logoutCookiePath)
                .build();
        Session session = this.sessionBuilderProvider.get()
                .sessionKey(sessionKey)
                .secondsToExpiration(this.sessionLifeSeconds)
                .build();
        user.addSession(session);
        return new CookieFromScratch[]{
                userIdTokenCookie,
                sessionTokenCookie,
                userIdLogoutCookie,
                sessionLogoutCookie
        };
    }
}
