package com.gitlab.akluball.chessapp.user.service;

import com.gitlab.akluball.chessapp.user.data.UserDao;
import com.gitlab.akluball.chessapp.user.exception.Http401;
import com.gitlab.akluball.chessapp.user.exception.Http404;
import com.gitlab.akluball.chessapp.user.model.User;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Stateless
public class LogoutService {
    private UserDao userDao;

    public LogoutService() {
    }

    @Inject
    public LogoutService(UserDao userDao) {
        this.userDao = userDao;
    }

    public void logout(
            @NotNull(payload = Http401.Payload.class) Integer userId,
            @NotBlank(payload = Http401.Payload.class) String sessionKey
    ) {
        User user = this.userDao.findById(userId);
        if (Objects.isNull(user)) {
            throw new Http404(String.format("User with id %s", userId));
        }
        user.invalidateSession(sessionKey);
    }
}
