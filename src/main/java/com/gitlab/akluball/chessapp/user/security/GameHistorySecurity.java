package com.gitlab.akluball.chessapp.user.security;

import com.gitlab.akluball.chessapp.user.UserConfig;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.SignedJWT;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class GameHistorySecurity {
    private RSASSAVerifier verifier;

    @Inject
    GameHistorySecurity(UserConfig userConfig) {
        this.verifier = new RSASSAVerifier(userConfig.gameHistoryPublicKey());
    }

    public boolean isValidSignature(SignedJWT token) {
        try {
            return token.verify(this.verifier);
        } catch (JOSEException e) {
            return false;
        }
    }
}
