package com.gitlab.akluball.chessapp.user;

import javax.inject.Singleton;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Objects;

import static com.gitlab.akluball.chessapp.user.util.Util.hoursToSeconds;
import static com.gitlab.akluball.chessapp.user.util.Util.minutesToSeconds;

@Singleton
public class UserConfig {
    private final PrivateKey userPrivateKey;
    private final RSAPublicKey userPublicKey;
    private final RSAPublicKey gameHistoryPublicKey;
    private final int sessionLifeSeconds;
    private final int tokenLifeSeconds;

    UserConfig() throws NoSuchAlgorithmException, InvalidKeySpecException {
        Base64.Decoder decoder = Base64.getDecoder();
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        byte[] userPrivateKeyBytes = decoder.decode(System.getenv("CHESSAPP_USER_PRIVATEKEY"));
        this.userPrivateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(userPrivateKeyBytes));
        byte[] userPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_USER_PUBLICKEY"));
        this.userPublicKey = (RSAPublicKey) keyFactory.generatePublic(new X509EncodedKeySpec(userPublicKeyBytes));
        byte[] gameHistoryPublicKeyBytes = decoder.decode(System.getenv("CHESSAPP_GAMEHISTORY_PUBLICKEY"));
        this.gameHistoryPublicKey = (RSAPublicKey) keyFactory
                .generatePublic(new X509EncodedKeySpec(gameHistoryPublicKeyBytes));
        this.sessionLifeSeconds = fromEnvOrDefault("CHESSAPP_USER_SESSIONLIFESECONDS", hoursToSeconds(24));
        this.tokenLifeSeconds = fromEnvOrDefault("CHESSAPP_USER_TOKENLIFESECONDS", minutesToSeconds(5));
    }

    private static int fromEnvOrDefault(String env, int defaultTo) {
        String fromEnv = System.getenv(env);
        return (Objects.nonNull(fromEnv)) ? Integer.parseInt(fromEnv) : defaultTo;
    }

    public PrivateKey userPrivateKey() {
        return this.userPrivateKey;
    }

    public RSAPublicKey userPublicKey() {
        return this.userPublicKey;
    }

    public RSAPublicKey gameHistoryPublicKey() {
        return this.gameHistoryPublicKey;
    }

    public int sessionLifeSeconds() {
        return this.sessionLifeSeconds;
    }

    public int tokenLifeSeconds() {
        return this.tokenLifeSeconds;
    }
}
