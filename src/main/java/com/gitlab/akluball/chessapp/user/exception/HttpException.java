package com.gitlab.akluball.chessapp.user.exception;

import javax.ejb.ApplicationException;
import javax.ws.rs.WebApplicationException;

@ApplicationException(inherited = true, rollback = true)
public class HttpException extends WebApplicationException {
    private final int status;
    private final String message;

    public HttpException(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
