package com.gitlab.akluball.chessapp.user.http;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

@RequestScoped
public class RequestUriExtractor {
    private URI requestUri;
    private String contextPath;

    public RequestUriExtractor() {
    }

    @Inject
    public RequestUriExtractor(HttpServletRequest httpServletRequest) throws URISyntaxException {
        this.requestUri = new URI(httpServletRequest.getRequestURL().toString());
        this.contextPath = httpServletRequest.getContextPath();
    }

    @Produces
    @RequestDomain
    public String domain() {
        String schemeAndHost = String.format("%s://%s", this.requestUri.getScheme(), this.requestUri.getHost());
        int port = this.requestUri.getPort();
        String portPostfix = (port == -1) ? "" : ":" + port;
        return schemeAndHost + portPostfix;
    }

    @Produces
    @RequestContextPath
    public String contextPath() {
        return this.contextPath;
    }
}
