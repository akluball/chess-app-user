package com.gitlab.akluball.chessapp.user.transfer;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;

@Schema(name = "GameResult")
public class GameResultDto {
    @NotNull(message = "GameResult.whiteSideUserId is required")
    private Integer whiteSideUserId;
    @NotNull(message = "GameResult.blackSideUserId is required")
    private Integer blackSideUserId;
    @NotNull(message = "GameResult.whiteSideUserRatingAdjustment is required")
    private Integer whiteSideUserRatingAdjustment;
    @NotNull(message = "GameResult.blackSideUserRatingAdjustment is required")
    private Integer blackSideUserRatingAdjustment;

    public int getWhiteSideUserId() {
        return this.whiteSideUserId;
    }

    public int getBlackSideUserId() {
        return this.blackSideUserId;
    }

    public int getWhiteSideUserRatingAdjustment() {
        return this.whiteSideUserRatingAdjustment;
    }

    public int getBlackSideUserRatingAdjustment() {
        return this.blackSideUserRatingAdjustment;
    }
}
