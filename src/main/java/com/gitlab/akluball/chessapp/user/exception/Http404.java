package com.gitlab.akluball.chessapp.user.exception;

public class Http404 extends HttpException {
    public Http404(String notFoundDescription) {
        super(404, String.format("not found: %s", notFoundDescription));
    }
}
