package com.gitlab.akluball.chessapp.user.security;

import com.gitlab.akluball.chessapp.user.http.RequestContextPath;

import javax.enterprise.context.Dependent;
import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Objects;

public class CookieFromScratch {
    private Integer maxAge;
    private String name;
    private String value;
    private String domain;
    private String path;

    public int getMaxAge() {
        return this.maxAge;
    }

    public String headerValue() {
        String headerValueBase = String.format("%s=%s; HttpOnly", this.name, this.value);
        StringBuilder headerValueBuilder = new StringBuilder(headerValueBase);
        if (Objects.nonNull(this.domain)) {
            headerValueBuilder.append(String.format("; Domain=%s", this.domain));
        }
        if (Objects.nonNull(this.path)) {
            headerValueBuilder.append(String.format("; Path=%s", this.path));
        }
        if (Objects.nonNull(this.maxAge)) {
            headerValueBuilder.append(String.format("; Max-Age=%s", this.maxAge));
        }
        return headerValueBuilder.toString();
    }

    @Dependent
    public static class Builder {
        private CookieFromScratch cookieFromScratch;
        private String contextPath;

        public Builder() {
        }

        @Inject
        public Builder(@RequestContextPath String contextPath) throws URISyntaxException {
            this.cookieFromScratch = new CookieFromScratch();
            this.contextPath = contextPath;
        }

        public Builder name(String name) {
            this.cookieFromScratch.name = name;
            return this;
        }

        public Builder value(String value) {
            this.cookieFromScratch.value = value;
            return this;
        }

        public Builder path(String path) {
            this.cookieFromScratch.path = this.contextPath + path;
            return this;
        }

        public Builder maxAgeSeconds(int seconds) {
            this.cookieFromScratch.maxAge = seconds;
            return this;
        }

        public CookieFromScratch build() {
            return this.cookieFromScratch;
        }
    }
}
