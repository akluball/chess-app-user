package com.gitlab.akluball.chessapp.user.controller;

import com.gitlab.akluball.chessapp.user.controller.openapi.OpenApiSecuritySchemes;
import com.gitlab.akluball.chessapp.user.controller.openapi.OpenApiTags;
import com.gitlab.akluball.chessapp.user.model.User;
import com.gitlab.akluball.chessapp.user.security.Bearer;
import com.gitlab.akluball.chessapp.user.security.BearerRole;
import com.gitlab.akluball.chessapp.user.security.CurrentUserId;
import com.gitlab.akluball.chessapp.user.security.ServiceName;
import com.gitlab.akluball.chessapp.user.service.UserService;
import com.gitlab.akluball.chessapp.user.transfer.GameResultDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.stream.Collectors;

@Path("user")
@RequestScoped
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Tag(name = OpenApiTags.USER)
@SecurityRequirement(name = OpenApiSecuritySchemes.BEARER)
public class UserController {

    private UserService userService;
    private Integer currentUserId;

    public UserController() {
    }

    @Inject
    public UserController(UserService userService, @CurrentUserId Integer currentUserId) {
        this.userService = userService;
        this.currentUserId = currentUserId;
    }

    @GET
    @Bearer(roles = BearerRole.USER)
    @Operation(summary = "Gets current user", description = "Gets current user identified by bearer token")
    @ApiResponse(
            responseCode = "200",
            description = "Current user",
            content = @Content(schema = @Schema(implementation = User.Dto.class))
    )
    @ApiResponse(responseCode = "401", description = "Invalid credentials")
    @ApiResponse(responseCode = "404", description = "User not found")
    public User.Dto getCurrentUser() {
        return this.userService.findById(this.currentUserId).toDto();
    }

    @GET
    @Path("{userId}")
    @Bearer(roles = { BearerRole.USER, BearerRole.SERVICE }, serviceNames = ServiceName.GAMEHISTORY)
    @Operation(summary = "Gets user", description = "Gets user by id")
    @ApiResponse(
            responseCode = "200",
            description = "Found user",
            content = @Content(schema = @Schema(implementation = User.Dto.class))
    )
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @ApiResponse(responseCode = "401", description = "Invalid credentials")
    @ApiResponse(responseCode = "404", description = "User not found")
    public User.Dto findById(@PathParam("userId") Integer userId) {
        return this.userService.findById(userId).toDto();
    }

    @GET
    @Path("search")
    @Bearer(roles = BearerRole.USER)
    @Operation(summary = "Searches for user", description = "Searches (case insensitive) for user by handle")
    @ApiResponse(
            responseCode = "200",
            description = "User search results",
            content = @Content(array = @ArraySchema(schema = @Schema(implementation = User.Dto.class)))
    )
    @ApiResponse(responseCode = "400", description = "Bad request")
    @ApiResponse(responseCode = "401", description = "Invalid credentials")
    public List<User.Dto> search(@QueryParam("searchString") String searchString) {
        return this.userService.search(searchString)
                .stream()
                .map(User::toDto)
                .collect(Collectors.toList());
    }

    @POST
    @Path("endgame")
    @Bearer(roles = BearerRole.SERVICE, serviceNames = ServiceName.GAMEHISTORY)
    @Operation(summary = "Handle end of game", description = "Perform end of game updates to participants")
    @ApiResponse(responseCode = "204", description = "End of game updates performed")
    @ApiResponse(responseCode = "400", description = "Bad request")
    @ApiResponse(responseCode = "404", description = "Not found")
    public void handleEndOfGame(GameResultDto gameResultDto) {
        this.userService.handleEndOfGame(gameResultDto);
    }
}
