package com.gitlab.akluball.chessapp.user.service;

import com.gitlab.akluball.chessapp.user.data.UserDao;
import com.gitlab.akluball.chessapp.user.exception.Http404;
import com.gitlab.akluball.chessapp.user.model.User;
import com.gitlab.akluball.chessapp.user.transfer.GameResultDto;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Stateless
public class UserService {

    private UserDao userDao;

    public UserService() {
    }

    @Inject
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    public User findById(@NotNull(message = "userId is required") Integer userId) {
        User user = this.userDao.findById(userId);
        if (Objects.isNull(user)) {
            throw new Http404(String.format("User with id %s", userId));
        }
        return user;
    }

    public List<User> search(@NotBlank(message = "searchString is required") String searchString) {
        return this.userDao.search(searchString);
    }

    public void handleEndOfGame(@NotNull(message = "GameResult is required") @Valid GameResultDto gameResultDto) {
        User whiteSideUser = this.userDao.findById(gameResultDto.getWhiteSideUserId());
        if (Objects.isNull(whiteSideUser)) {
            throw new Http404(String.format("User with id %s", gameResultDto.getWhiteSideUserId()));
        }
        User blackSideUser = this.userDao.findById(gameResultDto.getBlackSideUserId());
        if (Objects.isNull(blackSideUser)) {
            throw new Http404(String.format("User with id %s", gameResultDto.getBlackSideUserId()));
        }
        whiteSideUser.adjustRating(gameResultDto.getWhiteSideUserRatingAdjustment());
        blackSideUser.adjustRating(gameResultDto.getBlackSideUserRatingAdjustment());
    }
}
