package com.gitlab.akluball.chessapp.user.swagger;

import com.gitlab.akluball.chessapp.user.UserApplication;
import io.swagger.v3.jaxrs2.Reader;

import javax.ws.rs.ApplicationPath;
import java.util.Objects;

public class CustomReader extends Reader {
    @Override
    protected String resolveApplicationPath() {
        ApplicationPath applicationPathAnnotation = UserApplication.class
                .getAnnotation(ApplicationPath.class);
        if (Objects.nonNull(applicationPathAnnotation)) {
            return applicationPathAnnotation.value();
        } else {
            return super.resolveApplicationPath();
        }
    }
}
