# Chess Application User

This is the user microservice of the [chess-application].

## Test

Integration tests require jdk8 and docker.

Run integration test setup (starts payara and database container and deploys application):
```
./gradlew iTestSetup
```
This only needs to be run before the first test run (not before each).

Run integration tests (redeploys application at start):
```
./gradlew iTest
```

Run integration test teardown (undeploys application and stops containers):
```
./gradlew iTestTeardown
```
This only needs to be run after the last test run (not after each).

## Build

```
./gradlew war
```

## Expected Environment

The application expects the following variables set in the deployment environment:
1. `CHESSAPP_USER_PRIVATEKEY`
2. `CHESSAPP_USER_PUBLICKEY`
3. `CHESSAPP_GAMEHISTORY_PUBLICKEY`

The application can be further configured with the following optional environment variables:
1. `CHESSAPP_USER_SQL` (path to sql script)
2. `CHESSAPP_USER_SESSIONLIFESECONDS`
3. `CHESSAPP_USER_TOKENLIFESECONDS`

## Documentation

Create OpenAPI spec (written to `build/openapi.yaml`):
```
./gradlew resolve
```

## Notes

The test deployment uses postgres while the dev deployment uses embedded derby.

[chess-application]: https://gitlab.com/akluball/chess-app.git
